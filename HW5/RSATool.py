import sympy
import codecs

def encrypt(x, e, n):
    return pow(x, e, n)

def decrypt(y, d, n):
    return pow(y, d, n)


# code taken from https://www.geeksforgeeks.org/multiplicative-inverse-under-modulo-m/
# Iterative Python 3 program to find
# modular inverse using extended
# Euclid algorithm

# Returns modulo inverse of a with
# respect to m using extended Euclid
# Algorithm Assumption: a and m are
# coprimes, i.e., gcd(a, m) = 1
def modInverse(a, m) :
    m0 = m
    y = 0
    x = 1
    if (m == 1) :
        return 0
    while (a > 1) :
        # q is quotient
        q = a // m
        t = m

        # m is remainder now, process
        # same as Euclid's algo
        m = a % m
        a = t
        t = y
        # Update x and y
        y = x - q * y
        x = t

    # Make x positive
    if (x < 0) :
        x = x + m0

    return x


# Q3 ##################################
n=900348238029332668175881559281442423068410198618801978249457479037932527260331
e = 2**16 + 1
x = 39611541800605679895240898832274013906789
y = encrypt(x, e, n)
print('encrypted message y = ' + str(y))
#########################################


# Q4 #######################################
p = 929960838713976909720407931803976852619
q = 808840137850295240473898249118645909517
e = 2**16 + 1
n = p * q
print('n = ' + str(n))

phi_n = (p-1) * (q-1)
print('phi(n) = ' + str(phi_n))

cipherText = 159734044950403172452950888182831615189169981060911046776030041921878667289492
d = modInverse(e, (p-1)*(q-1))
print('d = ' + str(d))
plainText = decrypt(cipherText, d, n)
print('plain text decimal = ' + str(plainText))
hexText = hex(plainText)
print('hex text = ' + hexText)
asciiText = codecs.decode(hexText.replace('0x', ''), 'hex')
print('ascii text = ' + str(asciiText))
##############################################

# Q5 #####################################
n = 675165783015058999747250696593100898855524580839851357422716901254932757710763
e = 2**16 + 1
y = 284741839593698522184778360417494843180877037145726733862568997913589354461044
x = decrypt(y, e, n)
hexText = hex(x)
asciiText = codecs.decode(hexText.replace('0x', ''), 'hex')
print('ascii text = ' + str(asciiText))
########################################

# Q6 ###################################
n = 2**521 - 1
print('n is mersenne prime: ' + str(sympy.isprime(n)))
########################################
