import math
from collections import namedtuple

# code modified from https://stackoverflow.com/questions/31074172/elliptic-curve-point-addition-over-a-finite-field-in-python

# Create a simple Point class to represent the affine points.
Point = namedtuple("Point", "x y")

# The point at infinity (origin for the group law).
O = 'Origin'


def main():
    # q2-1
    P = Point(0, 4)
    print('(0,4) is on the curve: ' + str(valid(P, 2, 3, 13)))
    # q2-2
    points = generatePoints(0, 4, 2, 13)
    for point in points:
        print(point)

    # q3
    p = 13
    lb, ub = findHasse(p)
    print("number is in [" + str(lb) + str(',') + str(ub) + str(']'))

    # q4-1
    a = 654624412321892857559038596828572669649402987879847772735693306089759
    b = 563386056159714380473737077729260896240517015706612537779563193095411
    p = 1579602854473772853128287506817718026426265023617379175335587248616431
    x = 953216670857201615849458843136747040308850692140282160349854110301248
    y = 187696769665068572312633292858802066603155820538026405642457554453538
    n = 230768357842901099381188113760304602568543491144769691849643435691536

    lb, ub = findHasse(p)
    print("number is in [" + str(lb) + str(',') + str(ub) + str(']'))

    # q4-2
    x5, y5 = ng(x, y, a, p, n)
    print('ng-x = ' + str(x5))
    print('ng-y = ' + str(y5))


def valid(P, a, b, p):
    """
    Determine whether we have a valid representation of a point
    on our curve.  We assume that the x and y coordinates
    are always reduced modulo p, so that we can compare
    two points for equality with a simple ==.
    """
    if P == O:
        return True
    else:
        return (
            (P.y**2 - (P.x**3 + a*P.x + b)) % p == 0 and
            0 <= P.x < p and 0 <= P.y < p)

def ec_inv(P, p):
    """
    Inverse of the point P on the elliptic curve y^2 = x^3 + ax + b.
    """
    if P == O:
        return P
    return Point(P.x, (-P.y)%p)

def ec_add(P, Q, a, p):
    """
    Sum of the points P and Q on the elliptic curve y^2 = x^3 + ax + b.
    """
    if not (valid(P) and valid(Q)):
        raise ValueError("Invalid inputs")

    # Deal with the special cases where either P, Q, or P + Q is
    # the origin.
    if P == O:
        result = Q
    elif Q == O:
        result = P
    elif Q == ec_inv(P):
        result = O
    else:
        # Cases not involving the origin.
        if P == Q:
            dydx = (3 * P.x**2 + a) * modInverse(2 * P.y, p)
        else:
            dydx = (Q.y - P.y) * modInverse(Q.x - P.x, p)
        x = (dydx**2 - P.x - Q.x) % p
        y = (dydx * (P.x - x) - P.y) % p
        result = Point(x, y)

    # The above computations *should* have given us another point
    # on the curve.
    assert valid(result)
    return result


def modInverse(a, p) :
    if a % p == 0:
        raise ZeroDivisionError("Impossible inverse")
    return pow(a, p-2, p)


def ng(x, y, a, p, n):
    points = list()
    points.append((x, y))
    m = 2
    while m <= n:
        if y != 0:
            x3, y3 = pointDouble(x, y, a, p)
            if x3 is float('nan'):
                return x3, y3
            if m == n:
                return x3, y3
            else:
                points.append((x3, y3))
            if m*2 <= n:
                m *= 2
                x = x3
                y = y3
            else:
                while m <= n:
                    if m < n:
                        d = math.floor(math.log2(n - m))
                        x2, y2 = points[d]
                        if x3 != x2:
                            x5, y5 = pointAddition(x3, y3, x2, y2, p)
                            if x5 is float('nan'):
                                return x5, y5
                            m += pow(2, d)
                            x3 = x5
                            y3 = y5
                        else:
                            return x5, y5
                    else:
                        return x5, y5
        else:
            return float('nan'), float('nan')


def pointAddition(x1, y1, x2, y2, p):
    if x1 == x2:
        return float('nan'), float('nan')
    else:
        inverse = modInverse(x1 - x2, p)
    s = ((y2 - y1) * inverse) % p
    x3 = (s**2 - x1 - x2) % p
    y3 = (s * (x1 - x3) - y1) % p

    return x3, y3

def pointDouble(x1, y1, a, p):
    if y1 != 0:
        inverse = modInverse(2 * y1, p)
    else:
        return float('nan'), float('nan')
    s = ((3 * x1**2 + a) * inverse) % p
    x3 = (s**2 - 2 * x1) % p
    y3 = (s * (x1 - x3) - y1) % p

    return x3, y3

def findHasse(p):
    lb = p + 1 - math.floor(2 * math.sqrt(p))
    ub = p + 1 + math.floor(2 * math.sqrt(p))
    return lb, ub

def generatePoints(x1, y1, a, p):
    points = list()
    points.append((x1, y1))
    x2 = x1
    y2 = y1
    i = 2
    while True:
        if i == 2:
            if y1 != 0:
                x3, y3 = pointDouble(x1, y1, a, 13)
                if x3 is float('nan'):
                    points.append((x3, y3))
                    break
            else:
                break
        else:
            if x2 - x1 != 0:
                x3, y3 = pointAddition(x1, y1, x2, y2, p)
                if x3 is float('nan'):
                    points.append((x3, y3))
                    break
            else:
                break

        points.append((x3, y3))
        i = i + 1
        x1 = x3
        y1 = y3
    return points

if __name__ == '__main__':
    main()
