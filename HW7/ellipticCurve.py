import math



def main():
    # q1
    a = 654624412321892857559038596828572669649402987879847772735693306089759
    b = 563386056159714380473737077729260896240517015706612537779563193095411
    p = 1579602854473772853128287506817718026426265023617379175335587248616431
    x = 953216670857201615849458843136747040308850692140282160349854110301248
    y = 187696769665068572312633292858802066603155820538026405642457554453538

    alice_n = 814709178348331822963098404943044035246972495353080501869149056740241
    bob_n = 1016189342726403936529228449371555925007815563308599801179551629341341

    # q1-1
    xA, yA = find_ng(a, x, y, p, alice_n)
    print('xA = ' + str(xA))
    print('yA = ' + str(yA))
    print(validPoint(xA, yA, a, b, p))

    # q1-2
    xB, yB = find_ng(a, x, y, p, bob_n)
    print('xB = ' + str(xB))
    print('yB = ' + str(yB))
    print(validPoint(xB, yB, a, b, p))

    # q1-3
    x_T_AB_alice, y_T_AB_alice = find_ng(a, xB, yB, p, alice_n)
    print('x_T_AB_alice = ' + str(x_T_AB_alice))
    print('y_T_AB_alice = ' + str(y_T_AB_alice))
    print(validPoint(x_T_AB_alice, y_T_AB_alice, a, b, p))

    # q1-4
    x_T_AB_bob, y_T_AB_bob = find_ng(a, xA, yA, p, bob_n)
    print('x_T_AB_bob = ' + str(x_T_AB_bob))
    print('y_T_AB_bob = ' + str(y_T_AB_bob))
    print(validPoint(x_T_AB_bob, y_T_AB_bob, a, b, p))

    # q1-5
    sumX, sumY = add_p(xA, yA, xB, yB, p)
    print('sum of points = ' + str(sumX) + ', ' + str(sumY))
    print(validPoint(sumX, sumY, a, b, p))

    # q2-1 h(x) = 11, kE = 11
    p = 17
    q = 19
    d = 10
    a = 2
    b = 2
    x_A = 5
    y_A = 1
    k_E = 13
    h_x = 5
    # sign
    Bx, By = find_ng(a=a,x=x_A, y=y_A, p=p, n=d)
    print('B = ' + str(Bx) + ', ' + str(By)) # (7, 11)
    print(validPoint(Bx, By, a, b, p))

    Rx, Ry = find_ng(a=a, x=x_A, y=y_A, p=p, n=k_E)
    print('R = ' + str(Rx) + ', ' + str(Ry)) # (13, 10), (16, 4)
    print(validPoint(Rx, Ry, a, b, p))

    r = 16
    l = h_x + d * r
    k_E_inv = modInverse(k_E, q)
    s = ( l * k_E_inv) % q
    print('s = ' + str(s)) # 18, 1

    # verify
    w = modInverse(s, q)
    print('w = ' + str(w)) # 18, 1
    u1 = (w * h_x) % q
    print('u1 = ' + str(u1)) # 8, 5
    u2 = (w * r) % q
    print('u2 = ' + str(u2)) # 6, 16
    u1A = find_ng(a, x_A, y_A, p, u1)
    print('u1A = ' + str(u1A)) # (13, 7) (9, 16)
    print(validPoint(u1A[0], u1A[1], a, b, p))

    u2B = find_ng(a, 7, 11, p, u2)
    print('u2B = ' + str(u2B)) # (10, 6) (13, 7)
    print(validPoint(u2B[0], u2B[1], a, b, p))
    u1A_plus_u2B = add_p(9, 16, 13, 7, p)
    print(validPoint(u1A_plus_u2B[0], u1A_plus_u2B[1], a, b, p))
    print('u1A + u2B = ' + str(u1A_plus_u2B)) # (13, 10)


def validPoint(x, y, a, b, p):
    return (y**2 - (x**3 + a*x + b)) % p == 0 and 0<= x < p and 0 <= y < p


def modInverse(a, m) :
    m0 = m
    y = 0
    x = 1
    if (m == 1) :
        return 0
    while (a > 1) :
        # q is quotient
        q = a // m
        t = m

        # m is remainder now, process
        # same as Euclid's algo
        m = a % m
        a = t
        t = y
        # Update x and y
        y = x - q * y
        x = t

    # Make x positive
    if (x < 0) :
        x = x + m0

    return x


def find_ng(a, x, y, p, n):
    data = [(x, y)]
    m = 2
    while m <= n:
        if y != 0:
            x3, y3 = double_p(x, y, a, p)
            if m == n:
                return x3, y3
            data.append((x3, y3))
            if m*2 <= n:
                m *= 2
                x = x3
                y = y3
            else:
                while m <= n:
                    if m < n:
                        d = math.floor(math.log2(n - m))
                        x2, y2 = data[d]
                        if x3 != x2:
                            x5, y5 = add_p(x3, y3, x2, y2, p)
                            m += pow(2, d)
                            x3 = x5
                            y3 = y5
                        else:
                            return float('nan'), float('nan')
                    else:
                        return x5, y5
        else:
            return float('nan'), float('nan')

def add_p(x1, y1, x2, y2, md):
    if x2 > x1:
        inv = modInverse(x2 - x1, md)
    elif x2 < x1:
        inv = (-1) * modInverse(x1 - x2, md)
    else:
        return float('nan'), float('nan')
    s = ((y2 - y1) * inv) % md
    x3 = (s * s - x1 - x2) % md
    y3 = (s * (x1 - x3) - y1) % md
    return x3, y3


def double_p(x1, y1, a, md):
    if y1 != 0:
        inv = modInverse(y1*2, md)
    s = ((3*x1*x1 + a) * inv) % md
    x3 = (s * s - x1 - x1) % md
    y3 = (s * (x1 - x3) - y1) % md
    return x3, y3

def findHasse(p):
    lb = p + 1 - math.floor(2 * math.sqrt(p))
    ub = p + 1 + math.floor(2 * math.sqrt(p))
    return lb, ub


if __name__ == '__main__':
    main()
