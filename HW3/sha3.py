# modified from https://gist.github.com/danielbarkhorn/7a35709c0c2d14ec99f607b17e6bc569

SECURITY_LEVEL=64
b = 1600
nr = 24

def binaryToHex(num):
    asString = '0b' + str(num)
    asDecimal = int(asString, 2)
    return hex(asDecimal)


def decimalToBinary(num):
    return bin(num).replace("0b", "")

def hexToBinary(num):
    return decimalToBinary(int(num,16))

def convertBinaryNumberToList(num):
    asString = str(num)
    myList = list()
    for bit in asString:
        myList.append(int(bit))
    return myList

def convertBitListToBinaryNumber(bitList):
    asString = ''
    for bit in bitList:
        asString += str(bit)
    return int(asString)


def padInput(bitList):
    shaIn = [0] * 1600
    j = 0
    for i in range(len(bitList)-1, -1, -1):
        x = len(shaIn)-1 - 512 - j
        shaIn[x] = bitList[i]
        j+=1

    return shaIn


def arrTransform1(arr):
    output = [[[0 for _ in range(SECURITY_LEVEL)] for _ in range(5)] for _ in range(5)]

    for i in range(5):
        for j in range(5):
            for k in range(SECURITY_LEVEL):
                output[i][j][k] = arr[SECURITY_LEVEL * (5 * j + i) + k]

    return output

def arrTransform2(arr):
    output = [0]*b
    for i in range(5):
        for j in range(5):
            for k in range(SECURITY_LEVEL):
                output[SECURITY_LEVEL * (5 * j + i) + k] = arr[i][j][k]
    return output

def theta(A_in):
    A_out = [[[0 for _ in range(SECURITY_LEVEL)] for _ in range(5)] for _ in range(5)]
    for i in range(5):
        for j in range(5):
            for k in range(SECURITY_LEVEL):
                x1 = sum([A_in[i-1][jP][k] for jP in range(5)]) % 2
                x2 = sum([A_in[(i+1) % 5][jP][k-1] for jP in range(5)]) % 2
                s = x1 + x2 + A_in[i][j][k] % 2
                A_out[i][j][k] = s
    return A_out

#= ain[i][j][k − (t + 1)(t + 2)/2]
rhomatrix=[[0,36,3,41,18],[1,44,10,45,2],[62,6,43,15,61],[28,55,25,21,56],[27,20,39,8,14]]
def rho(A_in):
    A_out = [[[0 for _ in range(SECURITY_LEVEL)] for _ in range(5)] for _ in range(5)]
    for i in range(5):
        for j in range(5):
            for k in range(SECURITY_LEVEL):
                #t = RHO_MATRICES[(i,j)]
                b = A_in[i][j][k - rhomatrix[i][j]]
                A_out[i][j][k] = b

    return A_out

# aout[j'][2i′ + 3j′][k] = ain[i′][j′][k]
def pi(A_in):
    A_out = [[[0 for _ in range(SECURITY_LEVEL)] for _ in range(5)] for _ in range(5)]
    for i in range(5):
        for j in range(5):
            for k in range(SECURITY_LEVEL):
                A_out[j][(2*i + 3*j) %5][k] = A_in[i][j][k]
    return A_out

# aout[i][j][k] = ain[i][j][k] ⊕ ( (ain[i + 1][j][k] ⊕ 1)(ain[i + 2][j][k]) )
def chi(A_in):
    A_out = [[[0 for _ in range(SECURITY_LEVEL)] for _ in range(5)] for _ in range(5)]
    for i in range(5):
        for j in range(5):
            for k in range(SECURITY_LEVEL):
                or_one = (A_in[(i + 1)%5][j][k] + 1 )% 2
                or_one_mult = or_one * (A_in[(i + 2)%5][j][k])
                A_out[i][j][k] = (A_in[i][j][k] + or_one_mult) % 2
    return A_out

# aout[i][j][k] = ain[i][j][k] ⊕ bit[i][j][k]
# for 0 ≤ ℓ ≤ 6, we have bit[0][0][2ℓ − 1] = rc[ℓ + 7ir]
def iota(A_in, round):
    # generate rc
    A_out = A_in.copy()
    w=[1,0,0,0,0,0,0,0]
    rc = [w[0]]
    for _ in range(1,168):
        w=[w[1],w[2],w[3],w[4],w[5],w[6],w[7],w[0]+w[4]+w[5]+w[6]];
        rc.append(w[0])

    for l in range(7):
        A_out[0][0][2**l - 1] = (A_out[0][0][2**l - 1] + rc[l + 7*round]) % 2

    return A_out

#  ι ◦ χ ◦ π ◦ ρ ◦ θ
def sha3(A_in):
    roundOut = arrTransform1(A_in)
    for r in range(nr):
        roundOut = iota(chi(pi(rho(theta(roundOut)))), r)

    ans = arrTransform2(roundOut)
    return ans




def main():

    #hexInput = '0x6c36147652e71b560becbca1e7656c81b4f70bece26321d5e55e67a3db9d89e26f2f2a38fd0f289bf7fa22c2877e38d9755412794cef24d7b855303c332e0cb5e01aa50bb74844f5e345108d6811d5010978038b699ffaa370de8473f0cda38b89a28ed6cabaf6'
    #hexInput = '0x6c36147652'
    hexInput = '0x3A3A819C48EFDE2AD914FBF00E18AB6BC4F14513AB27D0C178A188B61431E7F5623CB66B23346775D386B50E982C493ADBBFC54B9A3CD383382336A1A0B2150A15358F336D03AE18F666C7573D55C4FD181C29E6CCFDE63EA35F0ADF5885CFC0A3D84A2B2E4DD24496DB789E663170CEF74798AA1BBCD4574EA0BBA40489D764B2F83AADC66B148B4A0CD95246C127D5871C4F11418690A5DDF01246A0C80A43C70088B6183639DCFDA4125BD113A8F49EE23ED306FAAC576C3FB0C1E256671D817FC2534A52F5B439F72E424DE376F4C565CCA82307DD9EF76DA5B7C4EB7E085172E328807C02D011FFBF33785378D79DC266F6A5BE6BB0E4A92ECEEBAEB1'
    #hexInput = '0xe9'

    binaryInput = hexToBinary(hexInput)

    binaryInputList = convertBinaryNumberToList(binaryInput)

    shaIn = padInput(binaryInputList)
    print('shaiIn = ' + str(shaIn))

    shaOut = sha3(shaIn)

    print('shaout = ' + str(shaOut))

    shaOutBinary = convertBitListToBinaryNumber(shaOut)

    print('sha out in binary = ' + str(shaOutBinary))

    shaOutHex = binaryToHex(shaOutBinary)

    print('sha out in hex = ' + str(shaOutHex))

    # f0d04dd1e6cfc29a4460d521796852f25d9ef8d28b44ee91ff5b759d72c1e6d6

    print('len of sha out in hex = ' + str(len(shaOutHex) -2))


    #print(shaOut[:16])

if __name__ == "__main__":
    main()
