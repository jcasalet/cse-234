import sympy

def main():
    n=2649376219191757686333291073027588009793925231566294290337286424331787812414080960960191670815548639

    # Q1: is n prime?
    print('n = ' + str(n) + ' is prime: ' + str(sympy.isprime(n)))

    # Q2: is n a safe prime?
    q = n // 2
    if sympy.isprime(q) and sympy.isprime(n):
        print('n is a safe prime')
    else:
        print('n is not a safe prime')

    # Q3: what is the sophie germain prime used to generate n
    q = n // 2

    p = n
    # Q4:
    fpg=list()
    for g in range(2, 21):
        if (pow(g, 2, p) != 1) and (pow(g, q, p) !=1):
            fpg.append(g)

    print('full period generators = ' + str(fpg))

if __name__ == "__main__":
    main()